export const revalidate = 60

import AllPosts from "@/app/api/posts/all/functions";

type PageInformation = {
    title: string,
    link: string,
    description: string
}

export async function GET() {
    var pageInformation: PageInformation = {
        title: 'JRehkemper.de',
        link: 'https://jrehkemper.de',
        description: 'Homepage and Wiki of Jannik Rehkemper'
    }
    /* start with template String */
    var rssString = `<?xml version="1.0"?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
<channel>
    <title>${pageInformation.title}</title>
    <link>${pageInformation.link}</link>
    <description>${pageInformation.description}</description>
    <language>en</language>`

    const wikiposts: Post[] = await AllPosts();
    rssString = AddItemsToRss(rssString, wikiposts, pageInformation)

    /* close open tags */
    rssString += `
</channel>
</rss>`

    return new Response(
        rssString,
        { 
            status: 200,
            headers: {
                'Content-Type': 'application/xml'
            } 
        }
    )
}

function AddItemsToRss(rssString: string, items: Post[], pageInformation: PageInformation) {
    items.forEach((post) => {
        rssString += `
        <item>
            <title>
                ${post.title}
            </title>
            <link>
                ${pageInformation.link}${post.parentpath}/${post.slug}
            </link>`
        if (post.mdcontent) {
            rssString += `
            <description>
                ${post.mdcontent.split("\n")[0]}
            </description>
            <pubDate>
                ${post.updated}
            </pubDate>
            `
        }
        rssString += `
        </item>
                `
    })

    return rssString
}