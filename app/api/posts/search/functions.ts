import { closeDB, getDB } from "@/lib/db";


export default async function SearchPosts(searchterm: string) {
    const db = getDB();

    const query = {
        text: `
            SELECT *
            FROM "posts"
            WHERE
                searchfield @@ to_tsquery('english', $1)
            AND
                "posttype" = 'post'
            ;`,
        values: [searchterm]
    }

    const res = await db.query(query);

    closeDB(db);

    const posts: Post[] = res.rows;
    return posts;
}