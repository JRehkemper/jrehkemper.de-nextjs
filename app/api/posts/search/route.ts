export const dynamic = 'force-dynamic'

import { NextResponse } from "next/server";
import SearchPosts from "./functions";


export async function POST(request: Request) {
    const body = await request.json();
    const posts: Post[] = await SearchPosts(body["searchterm"]);

    return NextResponse.json(posts);
}