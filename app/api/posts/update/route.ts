export const dynamic = 'force-dynamic';

import { closeDB, getDB } from "@/lib/db";
import { NextResponse } from "next/server";
import { Converter } from "showdown"


export async function POST(request: Request) {
    const body = await request.json();
    const db = getDB();
    const converter = new Converter();

    const query = {
        text: `
            UPDATE "posts"
            SET
                "updated" = CURRENT_TIMESTAMP,
                "mdcontent" = $1,
                "htmlcontent" = $2,
                "parentpath" = $4
            WHERE "slug" = $3;
        `,
        values: [
            body["md_content"],
            converter.makeHtml(body["md_content"]),
            body["slug"],
            body["parentpath"]
        ]
    }

    await db.query(query);

    closeDB(db);

    return new NextResponse(JSON.stringify({"msg": "ok"}))
}