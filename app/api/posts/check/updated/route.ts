export const dynamic = 'force-dynamic';

import { closeDB, getDB } from "@/lib/db";
import { NextResponse } from "next/server";


export async function POST(request: Request) {
    const body = await request.json();
    const db = getDB();

    const query = {
        text: `SELECT "id" FROM "posts"
            WHERE "updated"::date < $1::date AND "slug" = $2;`,
        values: [body["updated"], body["slug"]]
    }

    const res = await db.query(query);

    closeDB(db);

    return new NextResponse(JSON.stringify(res.rowCount));
}
