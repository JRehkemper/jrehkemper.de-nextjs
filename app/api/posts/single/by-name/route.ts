export const dynamic = 'force-dynamic';

import { closeDB, getDB } from "@/lib/db";
import { NextResponse } from "next/server";


export async function POST(request: Request) {
    const db = getDB();
    const body = await request.json();

    const query = {
        text: `
            SELECT "id" FROM "posts" WHERE "slug" = $1 AND "parentpath" = $2;`,
        values: [body["slug"], body["parentpath"]]
    }

    const res = await db.query(query);

    closeDB(db);

    return new NextResponse(JSON.stringify(res.rowCount));
}