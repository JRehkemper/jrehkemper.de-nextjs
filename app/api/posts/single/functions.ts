import { closeDB, getDB } from "@/lib/db";


export async function fetchSinglePost(url: string) {
    const slug = "/post" +url.split("/post")[1]
    const parentpath = url.split("/post")[0]

    const db = getDB();

    const query = {
        text: `
            SELECT * 
            FROM "posts"
            WHERE
                "slug" = $1 AND
                "parentpath" = $2 AND
                "posttype" = 'post'
            LIMIT 1;
            `,
        values: [slug, parentpath]
    }

    const res = await db.query(query)

    closeDB(db);


    const post: Post = res.rows[0]
    return post;
}

export async function fetchSingleDir(url: string) {
    const db = getDB();
    
    const query = {
        text: `
            SELECT * 
            FROM "posts"
            WHERE
                CONCAT("parentpath", "slug") = $1 AND
                "posttype" = 'dir'
            LIMIT 1;
            `,
        values: [url]
    }

    const res = await db.query(query)

    closeDB(db);

    const post: Post = res.rows[0]
    return post;
}
