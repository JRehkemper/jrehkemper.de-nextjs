export const dynamic = 'force-dynamic';

import { NextResponse } from "next/server";
import { fetchSinglePost } from "./functions";


export async function POST(request: Request) {
    const body = await request.json();

    const post = await fetchSinglePost(body["requestUrl"]);
    return NextResponse.json(post);
}