import { closeDB, getDB } from "@/lib/db";


export async function searchLinkHistory(url: string) {
    const db = getDB();

    const query = {
        text: `SELECT "new_link" FROM "link-history" WHERE "old_link" = $1;`,
        values: [url]
    }

    const res = await db.query(query);
    closeDB(db);

    if (res.rows[0]) {
        return res.rows[0];
    } else {
        return false;
    }
}