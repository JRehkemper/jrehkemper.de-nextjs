export const dynamic = 'force-dynamic';

import { closeDB, getDB } from "@/lib/db";
import { NextResponse } from "next/server";


export async function POST(request: Request) {
    const body = await request.json();
    const db = getDB();
    
    const query = {
        text: `SELECT "id" FROM "posts" WHERE "slug" = $1 AND "parentpath" = $2 AND "posttype" = 'dir';`,
        values: [body["slug"], body["parentpath"]]
    }

    let res = await db.query(query);

    if (res.rowCount == 0) {
        const query2 = {
            text: `
                INSERT INTO "posts" (
                    "posttype",
                    "title",
                    "slug",
                    "parentpath",
                    "updated"
                ) VALUES( $1, $2, $3, $4, CURRENT_DATE)`,
                values: [
                    'dir',
                    body["title"],
                    body["slug"],
                    body["parentpath"],
                    //body["updated"]
                ]
        }

        await db.query(query2);
    }

    closeDB(db);

    return new NextResponse(JSON.stringify({"msg": "ok"}))
}