export const dynamic = 'force-dynamic';

import { closeDB, getDB } from "@/lib/db";
import { NextResponse } from "next/server";
import { Converter } from "showdown"

export async function POST(request: Request) {
    const db = getDB();
    var body = await request.json();

    const converter = new Converter();

    var post: Post = {
        id: 0,
        author: body["author"],
        posttype: body["posttype"],
        title: body["title"],
        slug: body["slug"],
        parentpath: body["parentpath"],
        created: body["created"],
        updated: body["updated"],
        mdcontent: body["md_content"],
        htmlcontent: converter.makeHtml(body["md_content"])
    }

    const query = {
        text: `
            INSERT INTO "posts" (
                "author",
                "posttype",
                "title",
                "slug",
                "parentpath",
                "updated",
                "mdcontent",
                "htmlcontent"
            )
            VALUES(
                $1, $2, $3, $4, $5, CURRENT_TIMESTAMP, $6, $7
            )`,
        values: [
            post["author"],
            post["posttype"],
            post["title"],
            post["slug"],
            post["parentpath"],
            post["mdcontent"],
            post["htmlcontent"]
        ]
    }

    await db.query(query);

    closeDB(db);

    return new NextResponse(JSON.stringify({ "msg": "ok" }), { status: 200 })
}