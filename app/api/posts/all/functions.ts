import { closeDB, getDB } from "@/lib/db";


export default async function AllPosts() {
    const db = getDB();

    const query = {
        text: `
            SELECT *
            FROM "posts"
            WHERE "posttype" = 'post'
            ;
        `,
    }

    const res = await db.query(query);

    closeDB(db);    

    const posts: Post[] = res.rows;
    return posts;
}