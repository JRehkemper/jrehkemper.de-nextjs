import { closeDB, getDB } from "@/lib/db";


export default async function AllPostsByPath(parentpath: string) {
    const db = getDB();

    const query = {
        text: `
            SELECT *
            FROM "posts"
            WHERE
                "parentpath" LIKE $1
            ORDER BY
                CONCAT("parentpath", "slug")
            ;
        `,
        values: [parentpath+'%']
    }

    const res = await db.query(query);

    closeDB(db);

    const posts: Post[] = res.rows;
    return posts;
}