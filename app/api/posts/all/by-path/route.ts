export const revalidate = 60;

import { NextResponse } from "next/server";
import AllPostsByPath from "./functions";


export async function POST(request: Request) {
    const body = await request.json();
    const posts = await AllPostsByPath("/Content");
    return NextResponse.json(posts);
}