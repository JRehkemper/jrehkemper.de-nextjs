export const revalidate = 60;

import { NextResponse } from "next/server";
import AllPostsByDate from "./functions";


export async function GET() {
    const posts: Post[] = await AllPostsByDate(10);
    
    return NextResponse.json(posts);
}