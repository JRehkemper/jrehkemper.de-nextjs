import { closeDB, getDB } from "@/lib/db";


export default async function AllPostsByDate(amount: number) {
    const db = getDB();

    const query = {
        text: `
            SELECT * 
            FROM "posts"
            WHERE "posttype" = 'post'
            ORDER BY "created" DESC
            LIMIT $1;`,
        values: [amount]
    };

    const res = await db.query(query)

    closeDB(db);

    const posts: Post[] = res.rows;
    return posts;
}