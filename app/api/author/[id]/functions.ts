import { closeDB, getDB } from "@/lib/db";


export default async function GetAuthor(id: number) {
    const db = getDB();

    const query = {
        text: `
            SELECT * 
            FROM "authors" 
            WHERE "id" = $1
            ;`,
        values: [id]
    }

    const res = await db.query(query);

    closeDB(db);

    const author: Author = res.rows[0];
    return author;
}