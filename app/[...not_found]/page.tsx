import { headers } from "next/headers";
import {notFound} from "next/navigation"
import { searchLinkHistory } from "../api/posts/link-history/functions";
import { NextResponse } from "next/server";

export default async function NotFoundCatchAll() {
  //const linkHistory = await checkLinkHistory();
  //if (linkHistory == false) {
    notFound();
  //} else {
  //  return NextResponse.redirect("https://jrehkemper.de"+linkHistory["new_link"])
  //}
}

async function checkLinkHistory() {
  const headerlist = headers();
  const referer = headerlist.get("x-url");
  const url = referer?.replace('http://localhost:3000', '').replace('https://jrehkemper.de', '');
  if (!url) {
    return false;
  }

  const linkHistory = await searchLinkHistory(url);
  return linkHistory;
}