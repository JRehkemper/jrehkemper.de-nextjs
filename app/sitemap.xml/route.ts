import AllPostsByDate from "../api/posts/all/by-date/functions";
import AllPostsByPath from "../api/posts/all/by-path/functions";


export async function GET() {
    const last_post = await AllPostsByDate(1);
    
    let sitemap = `
    <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <url>
        <loc>https://jrehkemper.de/</loc>
        <lastmod>`+last_post[0].created.toISOString()+`</lastmod>
    </url>
    `

    const all_posts = await AllPostsByPath("/");

    all_posts.forEach((post) => {
        if (post.posttype == 'post') {
            sitemap += `
            <url>
                <loc>https://jrehkemper.de`+post.parentpath+post.slug+`</loc>
                <lastmod>`+post.created.toISOString()+`</lastmod>
            </url>`
        }
    })

    sitemap += `</urlset>`

    return new Response(sitemap,
        { 
            headers: {
                'content-type': 'application/xml'
            }
        }
    )
}