import Header from "@/components/Headers";
import "./globals.css";
import type { Metadata } from "next";
import { Inter } from "next/font/google";
import Footer from "@/components/Footer";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
    title: "Home - JRehkemper.de",
    description: "Homepage and Wiki of Jannik Rehkemper",
};

export default function RootLayout({
    children,
}: {
    children: React.ReactNode;
}) {
    return (
        <html lang="en" className="">
            <body className={`${inter.className} scroll-smooth w-full`}>
                <Header />
                <main className="min-h-screen px-0 xl:px-12 w-full flex justify-center self-center ">{children}</main>
                <Footer />
            </body>
        </html>
    );
}
