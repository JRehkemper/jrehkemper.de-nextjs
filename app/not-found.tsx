import Link from 'next/link';
import { headers } from 'next/headers';
import { searchLinkHistory } from './api/posts/link-history/functions';
import { NextResponse } from 'next/server';

export default async function NotFound() {
  const linkHistory = await checkLinkHistory();
  if (linkHistory == false) {
    return (
      <article className='p-16'>
        <h1>I am sorry, but this link doe not exist.</h1>
      </article>
    )
  } else {
    console.log("redirect")
    //return NextResponse.redirect("https://jrehkemper.de"+linkHistory["new_link"])
    return (
      <article className='p-16'>
        <p>The page was moved <a href={ 'https://jrehkemper.de' +linkHistory["new_link"] }>here.</a></p>
      </article>
    )
  }
}


async function checkLinkHistory() {
  const headerlist = headers();
  const referer = headerlist.get("x-url");
  const url = referer?.replace('http://localhost:3000', '').replace('https://jrehkemper.de', '');
  if (!url) {
    return false;
  }

  const linkHistory = await searchLinkHistory(url);
  return linkHistory;
}