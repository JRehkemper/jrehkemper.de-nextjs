import { NextResponse } from "next/server";
import { closeDB, getDB } from '@/lib/db'
import { redirect } from "next/dist/server/api-utils";


export async function GET(request: Request, { params }: { params: { link: string }}) {
    const { link } = params;

    const db = getDB();

    const query = {
        text:   `SELECT 
                   * 
                FROM
                    "links"
                WHERE
                    "from" LIKE $1
                ;`,
        values: [link]
    };

    const res = await db.query(query);

    closeDB(db);
    if (res.rows.length > 0) {
        return NextResponse.redirect("https://jrehkemper.de"+res.rows[0]['to'])
    } else {
        return new Response("Not found", {
            status: 404
        })
    }
}