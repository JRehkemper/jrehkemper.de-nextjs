import { Metadata } from 'next';
import Image from 'next/image';

export const metadata: Metadata = {
    title: 'About - JRehkemper.de',
    description: 'All about Jannik Rehkemper'
}

export default function About() {
    return (
        <div className='px-4'>
            <div className="py-32 flex flex-col items-center">
                <Image id="about-profile-image" className='rounded-3xl mb-8' alt="Image of Jannik Rehkemper" src={"/images/profile/jannik/profile.png"} width={256} height={256}></Image>
                <h1 id="about-title" className="font-bold text-4xl md:text-6xl text-center">Jannik Rehkemper</h1>
                <div className="p-1" />
                <h2 id="about-subtitle" className="text-xl text-center font-light">Linux Administrator and Hobby Programmer</h2>
            </div>

            <div className="about-main-fade-in">
                <p>My training as an IT-Professional started in August 2019 and ended in Juli 2022.</p>
                <p>Since January 2023 I am working as an Linux Administrator.</p>
            </div>

            <div className="py-12" />

            <div className="about-main-fade-in">
                <h3 className="font-bold text-3xl text-center">Certificates</h3>
                <div className="flex justify-center gap-8">
                    <Image 
                        src="/images/about/red-hat-certified-system-administrator-rhcsa.png"
                        alt="RedHat Certified System Administrator Certificate"
                        width={250}
                        height={250}
                    />
                </div>
            </div>

            <div className="py-12" />

            <div className='about-main-fade-in'>
                <h3 className="font-bold text-3xl text-center">Experience in Tools and Technologies</h3>
                <ul className='list-disc pl-12 pt-4'>
                    <li>Docker</li>
                    <li>Kubernetes</li>
                    <li>Ansible</li>
                    <li>Red Hat Enterprise Linux</li>
                </ul>
            </div>

            <div className="py-12" />

            <div className="about-main-fade-in">
                <h3 className="font-bold text-3xl text-center">Experience in Programming Languages</h3>
                <ul className='list-disc pl-12 pt-4'>
                    <li>Javascript</li>
                    <li>Python</li>
                    <li>Golang</li>
                    <li>C#</li>
                    <li>Java</li>
                </ul>
            </div>

            <div className="py-12" />
            
        </div>
    );
}
