import ArticlePage from "@/components/ArticlePage";
import ListingPage from "@/components/ListingPage";
import { fetchSingleDir, fetchSinglePost } from "@/app/api/posts/single/functions";
import { Metadata } from "next";

export async function generateMetadata({ params }: any): Promise<Metadata> {
    let requestUrl = "/Content/" + params.post.join('/');
    let page;
    if (checkPostOrListing(params) == 'post') {
        page = await fetchSinglePost(requestUrl);
    }
    else {
        page = await fetchSingleDir(requestUrl);
    }
    return {
        title: `${page.title} - JRehkemper.de`,
    }
}

export default function ContentPage({ params }: { params: any }) {
    if (checkPostOrListing(params) == 'post') {
        return (
            <ArticlePage postslug={params.post} />
        );
    }
    else if (checkPostOrListing(params) == 'listing') {
        return (
            <ListingPage postslug={params.post} />
        )
    }
}

/*
 * Check if the second-to-last parameter is "post"
 * If yes return "post" else return "listing"
 */
function checkPostOrListing(params: any) {
    const slug = params["post"];
    if (slug[slug.length - 2] == 'post') {
        return 'post';
    } else {
        return 'listing';
    }
}