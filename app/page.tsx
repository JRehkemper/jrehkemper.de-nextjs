export const dynamic = 'force-dynamic'

import AllPostsByDate from './api/posts/all/by-date/functions'
import NewPostCard from '@/components/NewPostCard';

export default async function Home() {
  const newPosts: Post[] = await AllPostsByDate(10);
  return (
    <div className="w-screen flex flex-col items-center">
      <div className='w-full flex flex-col items-center '>
        <h1 id="home-banner-title" className='
          font-bold 
          text-4xl md:text-6xl xl:text-7xl 
          pt-24 xl:pt-32 
          pb-0 xl:pb-4'
        >JRehkemper.de</h1>
        <h2 id="home-banner-subtitle" className='
          text-md md:text-xl xl:text-2xl 
          pb-32
        '>- Homepage und Wiki of Jannik Rehkemper -</h2>
      </div>

      <div id="home-banner-border" className='w-full border-b border-solid border-white' />

      <div className='w-full flex justify-center gap-4 pt-8'>
        <div id="home-new-posts-container" className='flex flex-col justify-center px-8 max-w-5xl'>
          <h3 id="home-new-post-title" className='text-2xl text-center pb-8 font-bold'>New Posts</h3>
          {newPosts.map((post, index) => {
            return (
              <NewPostCard post={post} key={post.id} index={index}/>
            )
          })}
        </div>

        <div className=''>
        </div>
      </div>

      <div className='w-8 py-8'></div>

    </div>
  )
}
