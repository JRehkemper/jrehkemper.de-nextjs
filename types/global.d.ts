type Post = {
    id: number,
    author: number,
    posttype: string,
    title: string,
    slug: string,
    parentpath: string,
    created: Date,
    updated: Date,
    mdcontent: string,
    htmlcontent: string
}

type Wikipost = {
    id: number,
    title: string,
    slug: string,
    parentpath: string,
    type: string,
    content: string,
    created: Date,
    updated: Date,
    excerpt: string,
    searchfield: string,
    partentid: number,
    rawcontent: string,
    author: number
}

type HierarchieElement = {
    id: number,
    title: string,
    slug: string,
    parentpath: string,
    posttype: string,
    htmlcontent: string,
    created: Date,
    updated: Date,
    mdcontent: string,
    author: number
    depth: number
}

type TocElement = {
    id: string,
    title: string,
    class: string
}

type Author = {
    id: number,
    name: string,
    text: string,
    imageurl: string
}
