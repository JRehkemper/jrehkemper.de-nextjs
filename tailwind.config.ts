import type { Config } from 'tailwindcss'

const config: Config = {
  content: [
    './pages/**/*.{js,ts,jsx,tsx,mdx}',
    './components/**/*.{js,ts,jsx,tsx,mdx}',
    './app/**/*.{js,ts,jsx,tsx,mdx}',
  ],
  theme: {
    extend: {
      backgroundImage: {
        'gradient-radial': 'radial-gradient(var(--tw-gradient-stops))',
        'gradient-conic':
          'conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))',
      },
      colors: {
        "background-color-01": "var(--background-color-01)",
        "font-color-01": "var(--font-color-01)",
        "font-color-02": "var(--font-color-02)",
        "font-accent-01": "var(--font-accent-01)",
      }
    },
  },
  plugins: [],
}
export default config
