import AllPostsByPath from "@/app/api/posts/all/by-path/functions";


type Props = {
    postslug: string[]
}

export default async function ListingPage({ postslug }: Props) {
    const parentpath = '/Content/' + postslug.join('/');
    const posts = await AllPostsByPath(parentpath);
    const postHierarchie = createHierarchie(posts, postslug);
    return (
        <div className="flex flex-col p-4 fade-in-animation">
            <h1 className="text-4xl text-center font-bold py-8">{postslug.at(-1)}</h1>
            <div dangerouslySetInnerHTML={{__html: postHierarchie}}></div>
        </div>
    )
}

function createHierarchie(posts: Post[], postslug: string[]) {
    var listOfElements: HierarchieElement[] = [];
    posts.forEach(element => {
        var hierarchieElement: HierarchieElement = {
            ...element,
            depth: 0
        };
        const currentPathName = postslug.join('/')
        const pathDifference = element.parentpath.replace('/Content/'+currentPathName, '');
        const numberOfSlashes = pathDifference.split('/').length;
        hierarchieElement.depth = numberOfSlashes;
        listOfElements.push(hierarchieElement)
    })

    var htmlString = "";
    var lastDepth = 0;

    for (let i = 0; i < listOfElements.length; i++) {
        /* assign short variable name */
        const post = listOfElements[i];

        /* get difference to previous post */
        const depth_diff =  post.depth - lastDepth

        /* add additional indentations */
        for (let k = 0; k < depth_diff; k++) {
            htmlString += `<ul class="list-depth-${post.depth} list-disc pb-4">`
        }

        /* Add Element itself */
        htmlString += `<li><a href="${post.parentpath}/${post.slug}">${post.title}</a></li>`

        /* Get depth difference to next post */
        var next_diff = 0
        /* catch array end */
        if (i+1 < listOfElements.length) {
            next_diff = post.depth - listOfElements[i+1].depth
        }
        for (var j = 0; j < next_diff; j++) {
            htmlString += `</ul>`
        }

        /* save depth of post for next iteration */
        lastDepth = post.depth
    }

    return htmlString    
}
