import Link from "next/link";
import Search from "./Search";
import MobileNavBar from "./MobileNavbar";


export default function Header() {
    return (
        <header id="header" className="w-full flex justify-around lg:justify-center gap-8 md:gap-4 lg:gap-24 items-center px-16 pt-6 pb-6 sticky top-0 backdrop-blur-md backdrop-brightness-50 border-b border-white border-solid z-20">
            <MobileNavBar />
            <Link href="/" prefetch className="flex justify-center gap-4 items-center">
                <h1 className="font-bold text-2xl">JRehkemper.de</h1>
            </Link>
            <div className="justify-around gap-8 items-center hidden md:flex">
                <Link href="/Content/Blog" prefetch className="transition-all">Blog</Link>
                <Link href="/Content/Databases" prefetch className="transition-all">Databases</Link>
                <Link href="/Content/Hardware" prefetch className="transition-all">Hardware</Link>
                <Link href="/Content/Linux" prefetch className="transition-all">Linux</Link>
                <Link href="/Content/Programming" prefetch className="transition-all">Programming</Link>
                <Link href="/about" prefetch className="transition-all">About</Link>
            </div>
            <Search />
        </header>
    )
}