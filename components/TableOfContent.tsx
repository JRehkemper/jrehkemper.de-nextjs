import Link from "next/link";

type Props = {
    content: string,
    title: string
}

export default function TableOfContent(props: Props) {
    const toc = createTableOfContent(props);
    return (
        <aside id="article-toc" className="mt-8 sticky top-44">
            <h1 className="pl-4 pb-2 font-bold text-2xl">Content</h1>
            <div className="border-l border-solid border-slate-100 border-font-accent-01">
                <ul>
                    {toc.map(line => { return (
                        <li className={`${line.class} pb-2`} key={line.id}><Link href={`#${line.id}`}>{ line.title }</Link></li>
                    )})}
                </ul>
            </div>

        </aside>
    )
}

function createTableOfContent(post: Props) {
    let table_of_contents: TocElement[] = [];

    let article = post.content;
    let lines = article.split('\n')

    lines.forEach(line => {
        if (line.startsWith("<h1")) {
            create_toc_element(line, table_of_contents, "h1")
        } else if (line.startsWith("<h2")) {
            create_toc_element(line, table_of_contents, "h2")
        } else if (line.startsWith("<h3")) {
            create_toc_element(line, table_of_contents, "h3")
        } else if (line.startsWith("<h4")) {
            create_toc_element(line, table_of_contents, "h4")
        } else if (line.startsWith("<h5")) {
            create_toc_element(line, table_of_contents, "h5")
        } else if (line.startsWith("<h6")) {
            create_toc_element(line, table_of_contents, "h6")
        }
    });
    return table_of_contents;
}

function create_toc_element(line: string, table_of_contents: TocElement[], tag: string) {
    const id = extract_id(line);
    const title = extract_title(line);
    const element: TocElement = {
        id: id,
        title: title,
        class: tag+"-toc"
    }
    table_of_contents.push(element);
}

function extract_id(text: string) {
    return text.split('"')[3]
}

function extract_title(text: string) {
    return text.split('>')[1].split('<')[0]
}