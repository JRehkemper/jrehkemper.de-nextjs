"use client"
import MenuIcon from "@mui/icons-material/Menu";
import Link from "next/link";
import { useState } from "react";

export default function MobileNavBar() {
    const [visible, setVisible] = useState(false);


    if (visible) {
        return (
            <>
                <button className="flex md:hidden" onClick={() => setVisible(!visible)}><MenuIcon /></button>
                <div id="mobile-navbar" className="absolute top-20 bottom-0 left-0 right-0 w-full h-fit rounded-b-2xl backdrop-brightness-50 backdrop-blur-md border-b border-solid border-white transition-all z-10">
                    <h3 className="text-center font-bold text-3xl py-8">Navigation</h3>
                    <div className="flex flex-col justify-around gap-8 items-center pb-8 ">
                        <h4><Link className="text-2xl" href="/Content/Blog" prefetch onClick={() => setVisible(!visible)}>Blog</Link></h4>
                        <h4><Link className="text-2xl" href="/Content/Databases" prefetch onClick={() => setVisible(!visible)}>Databases</Link></h4>
                        <h4><Link className="text-2xl" href="/Content/Hardware" prefetch onClick={() => setVisible(!visible)}>Hardware</Link></h4>
                        <h4><Link className="text-2xl" href="/Content/Linux" prefetch onClick={() => setVisible(!visible)}>Linux</Link></h4>
                        <h4><Link className="text-2xl" href="/Content/Programming" prefetch onClick={() => setVisible(!visible)}>Programming</Link></h4>
                        <h4><Link className="text-2xl" href="/about" prefetch onClick={() => setVisible(!visible)}>About</Link></h4>
                    </div>
                </div>
            </>
        )
    } 
    else {
        return (
            <button className="flex md:hidden" onClick={() => setVisible(!visible)}><MenuIcon /></button>
        )
    }
}