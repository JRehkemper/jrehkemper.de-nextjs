import Link from "next/link"

type Props = {
    post: Post,
    index: number
}

export default function NewPostCard({post, index}: Props) {
    if (post.mdcontent != null) {
        return (
            <Link href={`${post.parentpath}${post.slug}`} prefetch>
                <div id="home-new-post-card" style={{ animationDelay: `calc(300ms * ${index} + 1500ms)`}} className="pt-2 pb-4 xl:p-4 border-b border-solid border-white transition-all">
                    <h4 className="text-xl">{post.title}</h4>
                    <p className="text-sm pb-2">{post.parentpath.replaceAll('/', ' > ').replaceAll('-', ' ').replaceAll('   ', ' - ')}</p>
                    <p>{post.mdcontent.split("\n")[0]}</p>
                    <p className="pt-2">{post.created.toLocaleDateString()}</p>
                </div>
            </Link> 
        )
    }
}
