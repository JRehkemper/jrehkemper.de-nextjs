import GetAuthor from "@/app/api/author/[id]/functions";
import Image from "next/image";

type Props = {
    id: number;
}
export default async function Author({id}: Props) {
    const author = await GetAuthor(id);
    return (
        <div id="article-author" className="my-16 flex flex-col md:flex-row p-2 md:p-8 justify-start items-center border border-white border-solid rounded-3xl">
            <Image className="rounded-3xl" src={author.imageurl} alt="profile picture of the author" height={128} width={128} />
            <div className="px-4 md:pl-8 py-4">
                <h3 className="text-2xl text-center md:text-left">{author.name}</h3>
                <p className="leading-5">{author.text}</p>
            </div>
        </div>
    )
}