import Image from "next/image";
import Link from "next/link";

export default function Footer() {
    return (
        <footer className="flex w-full justify-center bottom-0">
            <div className="w-full flex flex-col items-center backdrop-blur-md backdrop-brightness-50 drop-shadow-2xl border-t border-white border-solid">
                <div className="flex justify-center gap-12 py-4">
                    <Link href="https://github.com/JRehkemper/">
                        <Image
                            className="svg-white"
                            src="/images/icons/svg/github.svg"
                            height={32}
                            width={32}
                            alt="GitHub Logo"
                        />
                    </Link>

                    <Link href="https://gitlab.com/JRehkemper">
                        <Image
                            className="svg-white"
                            src="/images/icons/svg/gitlab.svg"
                            height={32}
                            width={32}
                            alt="GitLab Logo"
                        />
                    </Link>

                    <Link href="mailto:system@jrehkemper.de">
                        <Image
                            className="svg-white"
                            src="/images/icons/svg/mail.svg"
                            height={32}
                            width={32}
                            alt="GitLab Logo"
                        />
                    </Link>

                    <Link href="/rss/jrehkemper.de.xml">
                        <Image
                            className="svg-white"
                            src="/images/icons/svg/rss.svg"
                            height={24}
                            width={24}
                            alt="RSS Feed Icon"
                        />
                    </Link>
                </div>
            </div>
        </footer>
    )
}