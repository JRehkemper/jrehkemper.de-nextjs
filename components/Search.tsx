"use client"
import SearchIcon from "@mui/icons-material/Search"
import CloseIcon from "@mui/icons-material/Close"
import { useState } from "react"
import Link from "next/link";


export default function Search() {
    const [visible, setVisible] = useState(false);
    const [posts, setPosts] = useState<Post[]>([])

    const closeSearchWindow = () => {
        setVisible(false)
    }

    const searchPosts = async (e: any) => {
        const searchTerm = e.target.value;
        
        /* Catch empty search */
        if (searchTerm.length < 1) {
            return;
        }

        const res = await fetch("/api/posts/search", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "searchterm": searchTerm
            })
        })
        const posts = await res.json()
        setPosts(posts);
    }

    if (visible) {
        return (
            <>
                <button className="flex" onClick={() => setVisible(!visible)}><SearchIcon /></button>
                <div className="absolute top-0 left-0 right-0 bottom-0 w-full h-screen flex flex-col justify-center items-center backdrop-brightness-50">
                    <div className="w-full md:w-3/4 xl:w-3/5 2xl:w-2/5 border border-solid border-white p-8 rounded-xl bg-black">
                        <div className="flex justify-end">
                            <button onClick={() => setVisible(false)}><CloseIcon /></button>
                        </div>
                        <h1 className="font-bold text-2xl text-center pb-4">Search</h1>
                        <input className="w-full" placeholder="Start typing..." onInput={(e) => searchPosts(e)}></input>
                        <div className="w-full border-b border-white border-solid pt-4 mb-4"></div>
                        <div className="flex flex-col gap-4">
                            {posts.map(post => {
                                return (
                                    <SearchResultPost post={post} key={post.id} closeSearchWindow={closeSearchWindow}/>
                                )
                            })}
                        </div>
                    </div>
                </div>
            </>
        )
    } else {
        return (
            <button className="flex" onClick={() => setVisible(!visible)}><SearchIcon /></button>
        )
    }
}

type Props = {
    post: Post,
    closeSearchWindow: () => void
}
function SearchResultPost({ post, closeSearchWindow }: Props) {
    return (
        <button onClick={() => closeSearchWindow()}>
            <Link href={`${post.parentpath}/${post.slug}`} prefetch>
                <div className="border border-white border-solid rounded-lg p-4">
                    <h4 className="font-bold">{post.title}</h4>
                    <p className="">{post.parentpath.replaceAll('/', ' > ')}</p>
                </div>
            </Link>
        </button>
    )
}