import { fetchSinglePost } from "@/app/api/posts/single/functions"
import { AddTailwindToHTML } from "@/lib/addTailwindToHTML"
import TableOfContent from "./TableOfContent"
import Author from "./Author"
import { Suspense } from "react"

type Props = {
    postslug: string[]
}

export default async function ArticlePage({postslug}: Props) {
    var requestUrl = "/Content/" + postslug.join('/')

    var page: Post;

    page = await fetchSinglePost(requestUrl);
    page.htmlcontent = AddTailwindToHTML(page.htmlcontent);
    return(
        <Suspense>

        <div className="flex md:grid md:grid-col-3 lg:grid-cols-5 p-4 pt-16 max-w-full">
            <div className="hidden lg:block"></div>

            <article className="
                break-after-column
                max-w-full md:max-w-2xl xl:max-w-3xl 
                col-span-3 
                leading-8 
                mx-0 md:mx-8 xl:mx-16
            ">
                <p id="article-updated" className="">Updated: {page.updated.toLocaleDateString() || "loading"}</p>
                <h1 id="article-title" className="text-3xl md:text-4xl lg:text-5xl font-bold pb-16">{page.title}</h1>
                <div id="article-content" dangerouslySetInnerHTML={{__html: page.htmlcontent}}></div>
                <Author id={page.author} />
            </article>

            <div className="hidden lg:block">
                <TableOfContent content={page.htmlcontent} title={page.title} />
            </div>
        </div>
        </Suspense>
    )
}