import { Client } from 'pg';

export function getDB() {
    const client = new Client({
        host: process.env.PG_HOST,
        port: Number(process.env.PG_PORT),
        database: process.env.PG_DATABASE,
        user: process.env.PG_USERNAME,
        password: process.env.PG_PASSWORD,
        application_name: 'JRehkemper.de'
    })

    client.connect();

    return client;
}

export function closeDB(client: Client) {
    client.end();
}