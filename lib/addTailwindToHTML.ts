

export function AddTailwindToHTML(content: string) {
    var content = content.replaceAll('<h2', '<h2 class="font-bold text-3xl pt-6 pb-5" ');
    var content = content.replaceAll('<h3', '<h3 class="font-bold text-2xl pt-4 pb-4" ');
    var content = content.replaceAll('<h4', '<h4 class="font-bold text-xl pt-4 pb-4" ');
    var content = content.replaceAll('<h5', '<h6 class="font-bold text-lg pt-4 pb-4" ');
    var content = content.replaceAll('<h6', '<h6 class="font-bold pt-4 pb-4" ');
    return content
}