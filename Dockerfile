FROM node:latest

ARG ARG_PG_PASSWORD
ENV PG_PASSWORD=$ARG_PG_PASSWORD

ENV PG_HOST=postgres-01.home
ENV PG_PORT=5432
ENV PG_USERNAME=jrehkemper.de
ENV PG_DATABASE=jrehkemper.de

WORKDIR /app
COPY . .

RUN npm install
RUN npm run build

EXPOSE 3000

CMD ["sh", "-c", "npm run start"]